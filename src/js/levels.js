const cache=Object.create(null);

const get=async filename => {
	if(cache[filename]) return cache[filename];
	const req=await fetch('levels/'+filename);
	const json=await req.json();
	return cache[filename]=json;
};

const getInfo=(category, id) => {
	const cat=levelList[category];
	id=''+id;

	const displayName=cat.levelDisplay
		.replace(/<n>/g, id)
		.replace(/<l>/g, id.toLowerCase());
	const fileName=cat.levelFilename
		.replace(/<n>/g, id)
		.replace(/<l>/g, id.toLowerCase());
	const levelString=category+'/'+id+'/'+fileName;

	return {
		displayName,
		fileName,
		levelString
	};
};

const getRules=async (category, id) => {
	const {fileName}=getInfo(category, id);
	const json=await get(fileName);
	return Object.assign({}, window.levelList[category].rules, json.rules);
};

const clearCache=() =>
	Object
		.keys(cache)
		.forEach(key => delete cache[key]);

return module.exports={
	get, getRules, getInfo,
	clearCache
};
