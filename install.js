const DB=require('better-sqlite3');
const fs=require('fs');
const child_process=require('child_process');

// prepare database
console.log('Preparing database');
if(fs.existsSync('db.sqlite')) fs.unlinkSync('db.sqlite');
const db=new DB('db.sqlite');
db.exec(fs.readFileSync('init.sql', 'utf8'));

// compile less
console.log('Compiling less');
child_process.execFileSync('node_modules/.bin/lessc', ['src/less/snek.less', 'public/css/snek.css']);

// merge js
console.log('Merging js');
const jsFiles=fs.readdirSync('src/js').map(f => 'src/js/'+f);
const merged=child_process.execFileSync('node', ['mergejs.js', ...jsFiles]);
fs.writeFileSync('public/js/snek.js', merged);
