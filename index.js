const express=require('express');

const app=express();
const PORT=process.env.PORT || 3000;

app.use(express.json());
app.use(express.static('public'));

app.use('/api', require('./api'));
app.get('/api/has-nodejs', (req, res) => {
	res.json("yes");
});

app.listen(PORT, () => {
	console.log(`Listening on 0.0.0.0:${PORT}`);
});
