.PHONY: all clean mrproper

SIZE = 32
TEMPSIZE = $(shell echo $(SIZE) '*4' | bc)

FIRE_ANIM = $(foreach angle, $(shell seq 0 6 359), build/fire$(angle).png)
PEACH_DECAY_ANIM = $(foreach percent, $(shell seq 99 -1 0), build/peach-decay$(percent).png)
PEACH_RAINBOW_ANIM = $(foreach percent, $(shell seq 100 2 299), build/peach-rainbow$(percent).png)
PORTAL_ANIM = $(foreach angle, $(shell seq 0 6 359), build/portal-a$(angle).png)

IMAGES = $(foreach name, apple wall oil key door, public/assets/$(name)$(SIZE).png)
TILESETS = $(foreach name, hole switch spikes, public/assets/$(name)-ts.png)
ANIMATIONS = $(foreach name, fire peach-decay peach-rainbow portal-a portal-b portal-c portal-d, public/assets/$(name)-anim.png)
JSON = $(foreach name, snake levelList config metaConfig, public/assets/$(name).json)
ICON = public/assets/icon32.png public/assets/icon256.png public/favicon.ico
CSS = public/css/snek.css
JS = public/js/snek.js

OUTPUT = $(IMAGES) $(TILESETS) $(ANIMATIONS) $(JSON) $(ICON) $(CSS) $(JS)

all: images tilesets animations json icon css js

images: $(IMAGES)
tilesets: $(TILESETS)
animations: $(ANIMATIONS)
json: $(JSON)
icon: $(ICON)
css: $(CSS)
js: $(JS)

public/favicon.ico: assets/icon.jpg
	convert $^ -resize 32x $@

public/assets/%32.png: assets/%.png
	convert $^ -resize 32x $@
public/assets/%256.png: assets/%.png
	convert $^ -resize 256x $@
public/assets/%$(SIZE).png: assets/%.png
	convert $^ -resize $(SIZE)x $@

public/assets/%32.png: assets/%.jpg
	convert $^ -resize 32x $@
public/assets/%256.png: assets/%.jpg
	convert $^ -resize 256x $@
public/assets/%$(SIZE).png: assets/%.jpg
	convert $^ -resize $(SIZE)x $@

build/%-smol.png: assets/%.png
	convert $^ -resize $(TEMPSIZE)x\> $@

public/assets/%-ts.png: assets/%.png
	convert $^ -scale $(SIZE)x $@

public/assets/fire-anim.png: $(FIRE_ANIM)
	convert $^ -append $@

build/fire%.png: build/fire-smol.png
	convert $^ -distort ScaleRotateTranslate $(shell echo $@ | sed 's/[^0-9]*//g') -resize $(SIZE)x $@

public/assets/peach-decay-anim.png: $(PEACH_DECAY_ANIM)
	convert $^ -append $@

build/peach-decay%.png: build/peach-smol.png
	convert $^ -modulate 100,$(shell echo $@ | sed 's/[^0-9]*//g') -resize $(SIZE)x $@

public/assets/peach-rainbow-anim.png: $(PEACH_RAINBOW_ANIM)
	convert $^ -append $@

build/peach-rainbow%.png: build/peach-smol.png
	convert $^ -modulate 100,100,$(shell echo $@ | sed 's/[^0-9]*//g') -resize $(SIZE)x $@

build/portal-a%.png: build/portal-smol.png
	convert $^ -distort ScaleRotateTranslate $(shell echo $@ | sed 's/[^0-9]*//g') -resize $(SIZE)x $@

public/assets/portal-a-anim.png: $(PORTAL_ANIM)
	convert $^ -append $@
public/assets/portal-b-anim.png: public/assets/portal-a-anim.png
	convert $^ -modulate 100,100,200 $@
public/assets/portal-c-anim.png: public/assets/portal-a-anim.png
	convert $^ -modulate 100,100,150 $@
public/assets/portal-d-anim.png: public/assets/portal-a-anim.png
	convert $^ -modulate 100,100,50 $@

public/assets/%.json: assets/%.json
	ln -s ../../$^ $@

public/css/snek.css: src/less/snek.less $(wildcard src/less/*.less)
	node_modules/.bin/lessc $< $@

public/js/snek.js: $(wildcard src/js/*.js)
	node mergejs.js $^ > $@

clean:
	rm -f build/*.*
mrproper: clean
	rm -f $(OUTPUT)
