const fs=require('fs');

const requireFn=`
const require=function require(name) {
	if(require.cache[name]) return require.cache[name];
	if(!require.source[name]) throw new Error("Cannot require "+name+": not found");
	require.cache[name]=require.source[name]({}) || true;
	return require.cache[name];
};
require.cache=Object.create(null);
require.source=Object.create(null);
window.require=require;
`;

let outputCode=[requireFn];
process.argv
	.slice(2)
	.map(a => [a, a.match(/([a-zA-Z_][a-zA-Z0-9_-]*).js$/)[1]])
	.forEach(([modFile, modName]) => {
		const modSource=fs.readFileSync(modFile, 'utf8');
		outputCode.push(`
require.source['${modName}']=(a => a.bind(a)) (function ${modName}(module) {
'use strict';
${modSource}
});
		`);
	});

fs.writeSync(1, outputCode.map(a => a.trim()).join('\n'));

