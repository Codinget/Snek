# Snek
![Snek icon, Miia from Mon Musu](assets/icon.jpg)

A "simple" Snake, done as my final JS class project

[Original subject](https://perso.liris.cnrs.fr/pierre-antoine.champin/enseignement/intro-js/s6.html)

## Features
- 60 FPS 2D animations
- arcade, speedrun and puzzle game modes
- touchscreen and controller support
- playable at [snek.codinget.me](https://snek.codinget.me)

## Dev dependencies
- All the POSIX tools, most importantly a POSIX-compliant shell, `echo`, `rm`, `seq` and `sed`
	- Busybox is known to work
	- GNU Coreutils are known to work
	- On Windows, WSL is known to work
- Imagemagick, with the `convert` tool in the PATH
- `bc`
- Make
- Node.js and npm, both in the PATH
	- Node.js 10 and 12 are known to work
	- node-gyp and python are required for the database
		- (if you have already used native modules, you have them)

## Prod dependencies (direct)
- Node.js and npm, both in the PATH
	- Node.js 10 and 12 are known to work
	- node-gyp and python are required for the database
		- (if you have already used native modules, you have them)

## Prod dependencies (docker)
- Docker

## Running the game (dev)
- `git clone` this repo
- `npm install` the dependencies (this will also build the less and js and initialize the database)
- `npm start` the server
- `make` every time you change something

## Running the game (prod, docker)
- Get the [Dockerfile](https://gitdab.com/Codinget/Snek/raw/branch/master/Dockerfile)
- `docker build` it
- `docker run -it -p80:3000` the container
- ideally, put it behind a reverse proxy

## Running the game (prod, direct)
- `git clone` this repo
- `npm install` the dependencies (this will also build the less and js and initialize the database)
- `npm start` the server

## License
[MIT](https://opensource.org/licenses/MIT)
