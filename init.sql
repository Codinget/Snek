CREATE TABLE leaderboards (
	mode TEXT,
	username TEXT,
	score INTEGER,
	length INTEGER,
	time INTEGER,
	speed INTEGER
);
